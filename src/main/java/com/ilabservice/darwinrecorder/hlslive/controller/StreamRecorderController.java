package com.ilabservice.darwinrecorder.hlslive.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.io.Files;
import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
import com.ilabservice.darwinrecorder.hlslive.config.MQTTConfiguration;
import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
//import com.ilabservice.darwinrecorder.hlslive.cron.DarwinListener;
import com.ilabservice.darwinrecorder.hlslive.entity.MQTTEnvelope;
import com.ilabservice.darwinrecorder.hlslive.entity.Video;
import com.ilabservice.darwinrecorder.hlslive.ffmpeg.FFMPEGUtils;
import com.ilabservice.darwinrecorder.hlslive.measurements.ErrorMeasurement;
import com.ilabservice.darwinrecorder.hlslive.measurements.RecorderMeasurement;
import com.ilabservice.darwinrecorder.hlslive.service.VideoService;
import com.ilabservice.darwinrecorder.hlslive.util.Handler;
import com.ilabservice.darwinrecorder.hlslive.util.RecorderRedisHandler;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.influxdb.dto.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.sound.midi.SysexMessage;
import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class StreamRecorderController {

    @Autowired
    Map<String, String> recordingList;

    @Autowired
    Handler handler;

    @Autowired
    FFMPEGUtils ffmpegUtils;

    @Value("${videoRecordPath}")
    private String videoRecordPath;

    @Value("${fileDownloadPath}")
    private String fileDownloadPath;

    @Value("${videoRecordPath-temp}")
    private String videoRecordPath_temp;

    @Value("${mqtt.producer.default-topic}")
    private String defaultTopic;

    @Value("${mqtt.producer.sendMessage}")
    private boolean sendMsg;

//    @Autowired
//    DarwinListener darwinListener;
    @Autowired
    InfluxDbUtils influxDbUtils;

    @Autowired
    List<String> allStreams;

    @Autowired
    List<String> allRTSP;

    @Autowired
    List<String> allRTSPInProgress;

    @Autowired
    private MQTTConfiguration.MqttGateway mqttGateway;

//    @Autowired
//    Map<String, Long> requestMap;
//
//    @Autowired
//    Map<String, String> stoppingMap;

    @Autowired
    VideoService videoService;

    @Autowired
    RecorderRedisHandler recorderRedisHandler;

//    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    private static final Logger log = LoggerFactory.getLogger(StreamRecorderController.class);

    @GetMapping("/video/remove/{stream}")
    public ResponseEntity removeRecorder(HttpServletRequest request, @PathVariable String stream){
        log.info("Stop recording stream " + stream + " forcibilly");
        RecorderMeasurement rm = new RecorderMeasurement();
        rm.setTime(System.currentTimeMillis());
        rm.setEvent("Force the stream to stop");
        rm.setType("videoRecorder");
        rm.setName(stream);
        rm.setRecordingCount(1);
        rm.setRecordedHours(0);
        rm.setRecords(0);
        Point point = Point.measurementByPOJO(rm.getClass()).addFieldsFromPOJO(rm).build();
        influxDbUtils.influxDB.write(point);
        recorderRedisHandler.removeRecorderStatus(stream);
        recorderRedisHandler.removeStoppingStatus(stream);
        return new ResponseEntity<String>("The stream  " + stream + " is been removed from recording", HttpStatus.OK);
    }


    @GetMapping("/video/recorder")
    public List<String> getRecordingList(){
        Map<String, Long> requestMap = recorderRedisHandler.getRecorderMap();
        Map<String, String> rtmp = recorderRedisHandler.getRTMPRecorderMap();
        if(requestMap != null){
            List<String> rtmpList = rtmp.keySet().stream().collect(Collectors.toList());
            List<String> rtsp = requestMap.keySet().stream().collect(Collectors.toList());
            List<String> retList = new ArrayList<String>();
            if(rtmpList!=null)
                retList.addAll(rtmpList);
            if(rtsp != null)
                retList.addAll(rtsp);

            return retList;
        }
        else
            return new ArrayList<>();

    }

//    //@Scheduled(cron = "0 0/3 * * * ? ")
//    private void removeUnnessaryRawReady(){
//        Map<String, Long> requestMap = recorderRedisHandler.getRecorderMap();
//        long retention = 1 * 60 * 60 * 1000L;
//        List<String> fetchList = allRTSP;
//        fetchList.stream().forEach(s->{
//            String recordPath = videoRecordPath_temp + File.separator +
//                    org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/") + "-manual" + File.separator;
//            File folder = new File(recordPath);
//            if(requestMap.entrySet().size()==0 || !requestMap.keySet().stream().anyMatch(ss->ss.equalsIgnoreCase(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/")))) {
//                new Thread(() ->{
//                    //remove unnecessary raw-ready
//                    //String currentFile = ffmpegUtils.currentFile(recordPath);
//                    if(folder !=null && folder.isDirectory()){
//                        Arrays.stream(folder.listFiles()).filter(sss->sss.isFile()&&sss.getName().endsWith("mp4")
//                                //&& !sss.getName().equalsIgnoreCase(currentFile)
//                                && sss.getName().contains("raw-ready")).forEach(sss->{
//                            log.info(s + "No record request ... Removing unnecessary segmentation file " + sss.getName());
//                            long created = Long.valueOf(StringUtils.substringBetween(sss.getName(),"raw-ready",".mp4"));
//                            long last_s = created + retention;
//                            if(last_s<System.currentTimeMillis())
//                                sss.delete();
//                        });
//                    }
//
//                }).start();
//            }
//        });
//    }

    //@Scheduled(cron = "0/10 * * * * ?")
    public void ring(String stream){
        Map<String, Long> requestMap = recorderRedisHandler.getRecorderMap();
        Map<String,String> stoppingMap = recorderRedisHandler.getStoppingMap();
        if(stream != null && !stream.isEmpty()&& !stoppingMap.keySet().contains(StringUtils.substringAfterLast(stream,"/")) &&
                !handler.streamLive(StringUtils.substringAfterLast(stream,"/"))){
            //&& !allRTSPInProgress.contains(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/"))

            log.info("ring recording for " + stream);
            if (!videoRecordPath_temp.endsWith(File.separator))
                videoRecordPath_temp = videoRecordPath_temp + File.separator;
            String recordPath = videoRecordPath_temp +
                    org.apache.commons.lang3.StringUtils.substringAfterLast(stream,"/") + "-manual" + File.separator;

            log.info("record file into the directory " + recordPath);
            File folder = new File(recordPath);

            if(!folder.exists())
                folder.mkdirs();
            //If the stream is not being recorded,then remove the raw ready segmentation
            if(requestMap.entrySet().size()==0||!requestMap.keySet().stream().anyMatch(ss->ss.equalsIgnoreCase(org.apache.commons.lang3.StringUtils.substringAfterLast(stream,"/")))) {
                new Thread(() -> {
                    if (requestMap.entrySet().size() == 0) {
                        if (folder != null && folder.isDirectory()) {
                            Arrays.stream(folder.listFiles()).filter(sss -> sss.isFile() && sss.getName().endsWith("mp4")
                                    && sss.getName().contains("raw-ready")).forEach(sss -> {
                                log.info("Removing unnecessary segmentation file>>>" + sss.getName());
                                sss.delete();
                            });
                        }
                    }
                }).start();
            }
            String file = recordPath;
            log.info("ring recording for " + recordPath +  "   >>> " +  file);
            ffmpegUtils.recordRaw(stream,file,org.apache.commons.lang3.StringUtils.substringAfterLast(stream,"/"));

        }


//        List<String> fetchList = darwinListener.getAllRTSP();
//        List<String> fetchList = new ArrayList<>();
//        fetchList.add("rtsp://qz.videostreaming.ilabservice.cloud:554/D72158832");

//        allRTSP.stream().filter(s-> ).forEach(s->{
//                    //&& !allRTSPInProgress.contains(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/"))
//
//            log.info("ring recording for " + s);
//            if (!videoRecordPath_temp.endsWith(File.separator))
//                videoRecordPath_temp = videoRecordPath_temp + File.separator;
//            String recordPath = videoRecordPath_temp +
//                    org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/") + "-manual" + File.separator;
//
//            File folder = new File(recordPath);
//
//            if(!folder.exists())
//                folder.mkdirs();
//            //If the stream is not being recorded,then remove the raw ready segmentation
//            if(requestMap.entrySet().size()==0||!requestMap.keySet().stream().anyMatch(ss->ss.equalsIgnoreCase(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/")))) {
//                new Thread(() -> {
//                    if (requestMap.entrySet().size() == 0) {
//                        if (folder != null && folder.isDirectory()) {
//                            Arrays.stream(folder.listFiles()).filter(sss -> sss.isFile() && sss.getName().endsWith("mp4")
//                                    && sss.getName().contains("raw-ready")).forEach(sss -> {
//                                log.info("Removing unnecessary segmentation file>>>" + sss.getName());
//                                sss.delete();
//                            });
//                        }
//                    }
//                }).start();
//            }
//            String file = recordPath;
//            log.info("ring recording for " + recordPath +  "   >>> " +  file);
//            ffmpegUtils.recordRaw(s,file,org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/"));
//
//        });
    }
    @ApiOperation("开始录像")
    @ApiResponses({ @ApiResponse(code = 200, message = "开始录像成功"),
            @ApiResponse(code = 400, message = "开始录像失败")})
    @ApiImplicitParams({ @ApiImplicitParam(paramType = "path", dataType = "String", name = "stream", value = "D70002092", required = true)
    })
    @GetMapping("/video/recordStart/{stream}")
    public ResponseEntity<String> markRecord(HttpServletRequest request, @PathVariable String stream){
        Map<String, Long> requestMap = recorderRedisHandler.getRecorderMap();
        if(requestMap.keySet().stream().anyMatch(s->s.contains(stream))){
            return new ResponseEntity<String>("The stream  " + stream + " is already being recorded", HttpStatus.BAD_REQUEST);
        }
        String recordPath = videoRecordPath_temp + File.separator +
                stream + "-manual" + File.separator;
        String currentFile = ffmpegUtils.currentFile(recordPath);

        //List<String> fetchList = darwinListener.getAllRTSP();
        if(!allRTSP.stream().anyMatch(s->s.contains(stream))){
            log.info("The stream is yet to live in darwin will revoke  >>>> " + stream);
            ErrorMeasurement em = new ErrorMeasurement();
            em.setTime(System.currentTimeMillis());
            em.setType("videoRecorder");
            em.setReason("stream is not live on darwin");
            em.setName(stream);
            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
            influxDbUtils.influxDB.write(point);
            recorderRedisHandler.saveRecorderStatus(stream,System.currentTimeMillis());
            if(new File(recordPath).exists())
            Arrays.stream(new File(recordPath).listFiles()).filter(s->s.isFile() && s.getName().startsWith("raw-ready") && s.getName().endsWith(".mp4") &&
                    !StringUtils.equalsIgnoreCase(s.getName(),currentFile)).forEach(s->{
                log.info("removing unnessary raw-ready file " + s.getName());
                s.delete();
            });
            return new ResponseEntity<String>("The stream is yet to live in darwin will revoke " + stream, HttpStatus.BAD_REQUEST);
        }


        Arrays.stream(new File(recordPath).listFiles()).filter(s->s.isFile() && s.getName().startsWith("raw-ready") && s.getName().endsWith(".mp4") &&
                !StringUtils.equalsIgnoreCase(s.getName(),currentFile)).forEach(s->{
            log.info("removing unnessary raw-ready file " + s.getName());
            s.delete();
        });

        recorderRedisHandler.saveRecorderStatus(stream,System.currentTimeMillis());

        RecorderMeasurement rm1 = new RecorderMeasurement();
        rm1.setTime(System.currentTimeMillis());
        rm1.setEvent("start to record stream");
        rm1.setType("videoRecorder");
        rm1.setName(stream);
        rm1.setRecordingCount(requestMap.keySet().size());
        rm1.setRecordedHours(0);
        rm1.setRecords(0);
        Point point = Point.measurementByPOJO(rm1.getClass()).addFieldsFromPOJO(rm1).build();
        influxDbUtils.influxDB.write(point);
        return new ResponseEntity<String>("The stream" + stream + " is being recorded", HttpStatus.OK);
    }


    @ApiOperation("停止录像")
    @ApiResponses({ @ApiResponse(code = 200, message = "停止录像成功"),
            @ApiResponse(code = 400, message = "停止录像失败")})
    @ApiImplicitParams({ @ApiImplicitParam(paramType = "path", dataType = "String", name = "stream", value = "D70002092", required = true)
    })
    @GetMapping("/video/recordStop/{stream}")
    public ResponseEntity markStop(HttpServletRequest request, @PathVariable String stream) {
        Map<String, Long> requestMap = recorderRedisHandler.getRecorderMap();
        Map<String, String> stoppingMap = recorderRedisHandler.getStoppingMap();
        if (!requestMap.keySet().stream().anyMatch(s -> s.contains(stream))) {
            return new ResponseEntity<String>("The stream" + stream + " has already been stopped", HttpStatus.OK);
        }

        if (stoppingMap.keySet().stream().anyMatch(s -> s.contains(stream))) {
            return new ResponseEntity<String>("The stream" + stream + " is in progress of stopping ...", HttpStatus.BAD_REQUEST);
        }

        if (!videoRecordPath_temp.endsWith(File.separator))
            videoRecordPath_temp = videoRecordPath_temp + File.separator;


        if (!videoRecordPath.endsWith(File.separator))
            videoRecordPath = videoRecordPath + File.separator;

        String recordPath_temp = videoRecordPath_temp +
                stream + "-manual" + File.separator;

        String recordPath = videoRecordPath +
                stream + "-manual" + File.separator;

        String segTime = "3600";

        if (!new File(recordPath).exists())
            new File(recordPath).mkdirs();

        RecorderMeasurement rm1 = new RecorderMeasurement();
        rm1.setTime(System.currentTimeMillis());
        rm1.setEvent("Start to stop record stream");
        rm1.setType("videoRecorder");
        rm1.setName(stream);
        rm1.setRecordingCount(1);
        rm1.setRecordedHours(0);
        rm1.setRecords(0);
        Point point1 = Point.measurementByPOJO(rm1.getClass()).addFieldsFromPOJO(rm1).build();
        influxDbUtils.influxDB.write(point1);

        Long statTime = requestMap.get(stream);
        Long endTime = System.currentTimeMillis();
        DecimalFormat df = new DecimalFormat("#.0");
        try {
            recorderRedisHandler.saveStoppingStatus(stream);
            handler.Stop(stream);
            //let the file to be written properlly...
        } catch (Exception e) {
            e.printStackTrace();
            ErrorMeasurement em = new ErrorMeasurement();
            em.setTime(System.currentTimeMillis());
            em.setType("videoRecorder");
            em.setReason("unable to stop record the stream");
            em.setName(stream);
            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
            influxDbUtils.influxDB.write(point);
        }

        if (statTime == null || statTime < 0) {
            recorderRedisHandler.removeStoppingStatus(stream);
            recorderRedisHandler.removeRecorderStatus(stream);
            return new ResponseEntity<String>("The stream " + stream + " has not startted yet, but now stopped  ", HttpStatus.BAD_REQUEST);
        }

        new Thread(() -> {
            String subFolder = recordPath + statTime + "-" + endTime + File.separator;
            String downloadPath = fileDownloadPath.endsWith(File.separator) ? fileDownloadPath + stream + "-manual" + File.separator + statTime + "-" + endTime + File.separator
                    : fileDownloadPath + File.separator + stream + "-manual" + File.separator + statTime + "-" + endTime + File.separator;
            String massageFolder = subFolder;
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (new File(recordPath_temp).exists()) {
                Arrays.stream(new File(recordPath_temp).listFiles()).filter(s -> s != null && s.isFile() && s.getName().endsWith(".mp4")
                        && s.getName().contains("raw-ready")).forEach(s -> {
                    try {
                        if (!new File(subFolder).exists())
                            new File(subFolder).mkdirs();
                        log.info("copying file " + s.getName() + " to " + massageFolder + File.separator);
                        //s.renameTo(new File(massageFolder + File.separator + s.getName()));
                        Files.copy(s, new File(massageFolder + File.separator + s.getName()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        ErrorMeasurement em = new ErrorMeasurement();
                        em.setTime(System.currentTimeMillis());
                        em.setType("videoRecorder");
                        em.setReason("can not copy file to destination");
                        em.setName(recordPath_temp + File.separator + s.getName());
                        Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                        influxDbUtils.influxDB.write(point);
                    }
                });
//let the copy done
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


            if (!new File(massageFolder).exists()) {
                recorderRedisHandler.removeRecorderStatus(stream);
                recorderRedisHandler.removeStoppingStatus(stream);
                return;
            } else {

                List<File> recordedFiles = Arrays.stream(new File(massageFolder).listFiles()).filter(s -> s != null && s.isFile() && s.getName().endsWith(".mp4")
                        && s.getName().contains("raw-ready")).collect(Collectors.toList());

                long firstTouch = statTime;
                long lastModif = 0;
                long firstFileStart = 0;
                if (recordedFiles != null && recordedFiles.size() > 0) {
                    Collections.sort(recordedFiles);
                    try {
                        int duration = 0;
                        String outputfile = subFolder + "readyfile";
                        try {
                            PrintWriter out = new PrintWriter(new BufferedWriter(
                                    new FileWriter(massageFolder + "index.txt", true)));


                            List<String> vaildFiles = new ArrayList<String>();
                            boolean indexing = false;
                            for (int i = 0; i < recordedFiles.size(); i++) {
                                String fName = recordedFiles.get(i).getName();
//                                if (i == 0)
//                                    firstFileStart = Long.valueOf(StringUtils.substringBetween(recordedFiles.get(i).getName(), "raw-ready", ".mp4"));
                                if(ffmpegUtils.validVideo(massageFolder+fName)){//only writes the valid files which is fbs > 50
                                    out.println("file " + fName);
                                    vaildFiles.add(fName);
                                    indexing=true;
                                }
                                else
                                    continue;
//                                if (i == recordedFiles.size() - 1)
//                                    lastModif = recordedFiles.get(i).lastModified();
                            }
                            if (out != null)
                                out.close();

                            if(!indexing){
                                log.info("could not found any valid file to be concat");
                                return;
                            }
                            if(vaildFiles.size()>0){
                                Collections.sort(vaildFiles);
                                firstFileStart = Long.valueOf(StringUtils.substringBetween(vaildFiles.get(0), "raw-ready", ".mp4"));
                                lastModif = new File(massageFolder + vaildFiles.get(vaildFiles.size() - 1)).lastModified();
                            }

                            String outputfile_temp = subFolder + "temp_" + statTime + "-" + endTime + ".mp4";
                            if (!new File(massageFolder + "index.txt").exists()) {
                                try {
                                    Thread.sleep(1000);
                                } catch (Exception e) {
                                }
                            }
                            ffmpegUtils.concat(massageFolder + "index.txt", outputfile_temp);
                            long lastM = new File(massageFolder + recordedFiles.get(recordedFiles.size() - 1).getName()).lastModified();
                            int totalDuration = ffmpegUtils.getVideoTime(outputfile_temp);
                            int startOffSet = getDuration(firstFileStart, statTime);
//
//                                    if (lastM < endTime)
//                                        startOffSet = totalDuration - getDuration(statTime, lastM);
//                                    else
//                                        startOffSet = totalDuration >= getDuration(statTime, endTime) ? totalDuration - getDuration(statTime, endTime) : 0;
                            if (startOffSet < 0) startOffSet = 0;
                            log.info("start offset is  " + startOffSet);
                            duration = totalDuration - startOffSet > getDuration(statTime, endTime) ? getDuration(statTime, endTime) : totalDuration - startOffSet;
                            log.info("duration is   " + duration);
                            ffmpegUtils.tailorLongVideo(outputfile_temp, outputfile, startOffSet, duration - 1, segTime);
                            File images = new File(subFolder);
                            List<File> readyFile = Arrays.stream(images.listFiles()).filter(s -> s.getName().startsWith("readyfile") && s.getName().endsWith("mp4")).collect(Collectors.toList());
                            Collections.sort(readyFile);
                            String name = "";
                            String raw = "";

                            if (readyFile.size() == 1) {
                                Video v = new Video();
                                v.setStartTime(firstTouch);
                                v.setEndTime(lastModif);
                                v.setCameraId(stream);
                                name = subFolder + firstTouch + "-" + lastModif;
                                log.info("renaming file " + readyFile.get(0).getName() + " to " + name + ".mp4");
                                raw = subFolder + readyFile.get(0).getName();
                                ffmpegUtils.getFirstFrameAsImage(raw, name + ".jpg");
                                readyFile.get(0).renameTo(new File(name + ".mp4"));
//                                v.setVideo(downloadPath + sdf.format(firstTouch) + "-" + sdf.format(lastModif) + ".mp4");
//                                v.setImage(downloadPath + sdf.format(firstTouch) + "-" + sdf.format(lastModif) + ".jpg");//
                                v.setVideo(downloadPath + firstTouch + "-" + lastModif + ".mp4");
                                v.setImage(downloadPath + firstTouch + "-" + lastModif + ".jpg");
                                v.setLength(((lastModif - firstTouch) / 1000));
                                v.setStatus(Constants.CREATED);
                                v.setSize(new File(raw).length());
                                v.setVaStartTime(System.currentTimeMillis());
                                videoService.save(v);

                                if (sendMsg) {
//                                    MQTTEnvelope mqttEnvelope = new MQTTEnvelope();
//                                    mqttEnvelope.setVideo(v);
                                    mqttGateway.sendToMqtt(defaultTopic, JSON.toJSONString(v));
                                }

                            } else for (int i = 0; i < readyFile.size(); i++) {
                                Video v = new Video();
                                v.setCameraId(stream);
                                if (i == readyFile.size() - 1) {
                                    name = subFolder + firstTouch + "-" + lastModif;
                                    log.info("renaming file " + readyFile.get(i).getName() + " to " + name + ".mp4");
                                    raw = subFolder + readyFile.get(i).getName();
                                    v.setStartTime(firstTouch);
                                    v.setEndTime(lastModif);
//                                    v.setVideo(downloadPath + sdf.format(firstTouch) + "-" + sdf.format(lastModif) + ".mp4");
//                                    v.setImage(downloadPath + sdf.format(firstTouch) + "-" + sdf.format(lastModif) + ".jpg");
                                    v.setVideo(downloadPath + firstTouch + "-" + lastModif + ".mp4");
                                    v.setImage(downloadPath + firstTouch + "-" + lastModif + ".jpg");
                                    v.setLength(((lastModif - firstTouch) / 1000));
                                } else {
                                    name = subFolder + firstTouch + "-" + (firstTouch + Long.valueOf(segTime) * 1000L);
                                    log.info("renaming file " + readyFile.get(i).getName() + " to " + name + ".mp4");
                                    raw = subFolder + readyFile.get(i).getName();
                                    v.setStartTime(firstTouch);
                                    v.setEndTime(firstTouch + Long.valueOf(segTime) * 1000L);
//                                    v.setVideo(downloadPath + sdf.format(firstTouch) + "-" + sdf.format(firstTouch + Long.valueOf(segTime) * 1000L) + ".mp4");
//                                    v.setImage(downloadPath + sdf.format(firstTouch) + "-" + sdf.format(firstTouch + Long.valueOf(segTime) * 1000L) + ".jpg");
                                    v.setVideo(downloadPath + firstTouch + "-" + (firstTouch + Long.valueOf(segTime) * 1000L) + ".mp4");
                                    v.setImage(downloadPath + firstTouch + "-" + (firstTouch + Long.valueOf(segTime) * 1000L) + ".jpg");
                                    v.setLength(Long.valueOf(segTime));

                                }
                                firstTouch = firstTouch + Long.valueOf(segTime) * 1000L;
                                ffmpegUtils.getFirstFrameAsImage(raw, name + ".jpg");
                                readyFile.get(i).renameTo(new File(name + ".mp4"));
                                v.setStatus(Constants.CREATED);
                                v.setVaStartTime(System.currentTimeMillis());
                                v.setSize(new File(raw).length());
                                videoService.save(v);
                                if (sendMsg) {
//                                    MQTTEnvelope mqttEnvelope = new MQTTEnvelope();
//                                    mqttEnvelope.setVideo(v);
                                    mqttGateway.sendToMqtt(defaultTopic, JSON.toJSONString(v));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            ErrorMeasurement em = new ErrorMeasurement();
                            em.setTime(System.currentTimeMillis());
                            em.setType("videoRecorder");
                            em.setReason("unable to merge recorded files");
                            em.setName(stream);
                            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                            influxDbUtils.influxDB.write(point);
                        }
                        RecorderMeasurement rm = new RecorderMeasurement();
                        rm.setTime(System.currentTimeMillis());
                        rm.setEvent("successfully stop record the stream");
                        rm.setType("videoRecorder");
                        rm.setName(stream);
                        rm.setRecords(1);
                        rm.setRecordingCount(requestMap.keySet().size());
                        rm.setRecordedHours(Double.valueOf(df.format(duration / 60)));
                        Point point = Point.measurementByPOJO(rm.getClass()).addFieldsFromPOJO(rm).build();
                        influxDbUtils.influxDB.write(point);

                    } catch (Exception e1) {
                        e1.printStackTrace();
                        ErrorMeasurement em = new ErrorMeasurement();
                        em.setTime(System.currentTimeMillis());
                        em.setType("videoRecorder");
                        em.setReason("unable to merge recorded files");
                        em.setName(stream);
                        Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                        influxDbUtils.influxDB.write(point);
                    }
//                    }

                    Arrays.stream(new File(recordPath_temp).listFiles()).filter(s -> s != null && s.isFile() && s.getName().endsWith(".mp4")
                            && s.getName().contains("raw-ready")).forEach(s -> s.delete());
                    recorderRedisHandler.removeStoppingStatus(stream);
                    recorderRedisHandler.removeRecorderStatus(stream);
                }
                ErrorMeasurement em = new ErrorMeasurement();
                em.setTime(System.currentTimeMillis());
                em.setType("videoRecorder");
                em.setReason("unable to merge recorded files");
                em.setName(stream);
                Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                influxDbUtils.influxDB.write(point);
                Arrays.stream(new File(recordPath_temp).listFiles()).filter(s -> s != null && s.isFile() && s.getName().endsWith(".mp4")
                        && s.getName().contains("raw-ready")).forEach(s -> s.delete());
                recorderRedisHandler.removeStoppingStatus(stream);
                recorderRedisHandler.removeRecorderStatus(stream);
            }
        }).start();
        return new ResponseEntity("Recording has been stopped for " + stream, HttpStatus.OK);

    }


    public int getDuration(long startTime, long endTime){
        long diff;
        if (startTime < endTime) {
            diff = endTime - startTime;
        } else {
            diff = 0;
        }

        return Long.valueOf(diff%1000==0?diff/1000:diff/1000+1).intValue();
    }

}
