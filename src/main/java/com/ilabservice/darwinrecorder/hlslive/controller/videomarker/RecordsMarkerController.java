package com.ilabservice.darwinrecorder.hlslive.controller.videomarker;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ilabservice.darwinrecorder.hlslive.entity.Video;
import com.ilabservice.darwinrecorder.hlslive.service.VideoService;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;

@RestController
public class RecordsMarkerController {

    @Autowired
    VideoService videoService;

    private static final Logger log = LoggerFactory.getLogger(RecordsMarkerController.class);

    @ApiOperation("录制视频标注")
    @ApiResponses({ @ApiResponse(code = 200, message = "标注成功"),
            @ApiResponse(code = 400, message = "标注失败")})
    @ApiImplicitParams({ @ApiImplicitParam(paramType = "body", dataType = "String", name = "cameraSN", value = "D70002092", required = true),
            @ApiImplicitParam(paramType = "body", dataType = "Long", name = "startTime", value = "1576744320000", required = true,allowMultiple=true)
    })
    @RequestMapping(value = "/records/mark", method = {RequestMethod.POST})
    public ResponseEntity evtVideoMarker(HttpServletRequest request){

        String requestJson = getRequestJson(request);
        JSONObject jsonObject = null;
        try {
            jsonObject = JSON.parseObject(requestJson);
        } catch (Exception e) {
            return new ResponseEntity("the request string in the request body is malformat json", HttpStatus.BAD_REQUEST);
        }

        if(jsonObject ==null)
            return new ResponseEntity("can not parse the request string to json object", HttpStatus.BAD_REQUEST);

        String cameraSN = "";
        if(!jsonObject.containsKey("cameraSN"))
            return new ResponseEntity("please provide the cameraSN", HttpStatus.BAD_REQUEST);
        cameraSN = jsonObject.getString("cameraSN");

        if(StringUtils.isEmpty(cameraSN))
            return new ResponseEntity("the cameraSN is empty", HttpStatus.BAD_REQUEST);

        log.info("handling camera  " + cameraSN);

        if(!jsonObject.containsKey("startTime"))
            return new ResponseEntity("please provide an array (startTime)", HttpStatus.BAD_REQUEST);

        JSONArray jsonArray = jsonObject.getJSONArray("startTime");

        if(jsonArray==null || jsonArray.size()==0)
            return new ResponseEntity("please provide value(s) for (startTime)", HttpStatus.BAD_REQUEST);


        for(int i=0;i<jsonArray.size();i++){
            Long startTime = (Long) jsonArray.get(i);
            log.info("handling the video with startTime " + startTime);
            Video video = videoService.findByCameraIdAndStartTime(cameraSN,startTime);
            if(video==null){
                log.info("can not find the matched video with startTime " + startTime + " and cameraSN " + cameraSN);
                continue;
            }
            video.setMarked(1);
            try {
                videoService.save(video);
            } catch (Exception e) {
                log.info("error occured during update the video with cameraSN " + cameraSN + " and startTime " + startTime);
                log.info(e.getMessage());
                continue;
            }
        }

        return new ResponseEntity("video(s) has/have been marked", HttpStatus.OK);
    }

    @ApiOperation("录制视频取消标注")
    @ApiResponses({ @ApiResponse(code = 200, message = "取消标注成功"),
            @ApiResponse(code = 400, message = "取消标注失败")})
    @ApiImplicitParams({ @ApiImplicitParam(paramType = "body", dataType = "String", name = "cameraSN", value = "D70002092", required = true),
            @ApiImplicitParam(paramType = "body", dataType = "Long", name = "startTime", value = "1576744320000", required = true,allowMultiple=true)
    })
    @RequestMapping(value = "/records/unmark", method = {RequestMethod.POST})
    public ResponseEntity evtVideoUnMarker(HttpServletRequest request){

        String requestJson = getRequestJson(request);
        JSONObject jsonObject = null;
        try {
            jsonObject = JSON.parseObject(requestJson);
        } catch (Exception e) {
            return new ResponseEntity("the request string in the request body is malformat json", HttpStatus.BAD_REQUEST);
        }

        if(jsonObject ==null)
            return new ResponseEntity("can not parse the request string to json object", HttpStatus.BAD_REQUEST);

        String cameraSN = "";
        if(!jsonObject.containsKey("cameraSN"))
            return new ResponseEntity("please provide the cameraSN", HttpStatus.BAD_REQUEST);
        cameraSN = jsonObject.getString("cameraSN");

        if(StringUtils.isEmpty(cameraSN))
            return new ResponseEntity("the cameraSN is empty", HttpStatus.BAD_REQUEST);

        log.info("handling camera  " + cameraSN);

        if(!jsonObject.containsKey("startTime"))
            return new ResponseEntity("please provide an array (startTime)", HttpStatus.BAD_REQUEST);

        JSONArray jsonArray = jsonObject.getJSONArray("startTime");

        if(jsonArray==null || jsonArray.size()==0)
            return new ResponseEntity("please provide value(s) for (startTime)", HttpStatus.BAD_REQUEST);


        for(int i=0;i<jsonArray.size();i++){
            Long startTime = (Long) jsonArray.get(i);
            log.info("handling the video with startTime " + startTime);
            Video video = videoService.findByCameraIdAndStartTime(cameraSN,startTime);
            if(video==null){
                log.info("can not find the matched video with startTime " + startTime + " and cameraSN " + cameraSN);
                continue;
            }
            video.setMarked(0);
            try {
                videoService.save(video);
            } catch (Exception e) {
                log.info("error occured during update the video with cameraSN " + cameraSN + " and startTime " + startTime);
                log.info(e.getMessage());
                continue;
            }
        }

        return new ResponseEntity("video(s) has/have been unmarked", HttpStatus.OK);
    }


    public String getRequestJson(HttpServletRequest request){
        BufferedReader bfr = null;
        StringBuilder sb = new StringBuilder("");
        try {
            bfr = request.getReader();
            String str;
            while ((str = bfr.readLine()) != null) {
                sb.append(str);
            }
            bfr.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != bfr) {
                try {
                    bfr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }
}
