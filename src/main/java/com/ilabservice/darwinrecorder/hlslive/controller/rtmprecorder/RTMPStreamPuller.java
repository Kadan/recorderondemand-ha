package com.ilabservice.darwinrecorder.hlslive.controller.rtmprecorder;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ilabservice.darwinrecorder.hlslive.controller.StreamRecorderController;
import com.ilabservice.darwinrecorder.hlslive.ffmpeg.FFMPEGUtils;
import com.ilabservice.darwinrecorder.hlslive.util.Handler;
import com.ilabservice.darwinrecorder.hlslive.util.RecorderRedisHandler;
import com.ilabservice.darwinrecorder.hlslive.util.RequestHanlder;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Arrays;
import java.util.Map;

@RestController
public class RTMPStreamPuller {

    @Value("${videoRecordPath-temp}")
    private String videoRecordPath_temp;

    @Autowired
    Handler handler;

    @Autowired
    FFMPEGUtils ffmpegUtils;

    @Autowired
    RecorderRedisHandler recorderRedisHandler;

    private static final Logger log = LoggerFactory.getLogger(RTMPStreamPuller.class);

    private static final String RTMP_SUFIX = "-rtmp";

    @Value("${rtmpRecordHistoricalMax}")
    private int rtmpRecordHistoricalMax;

    @ApiOperation("RTMP 拉流录制接口")
    @ApiResponses({ @ApiResponse(code = 200, message = "开始录制"),
            @ApiResponse(code = 400, message = "拉取RTMP流失败")})
    @ApiImplicitParams({ @ApiImplicitParam(paramType = "path", dataType = "String", name = "sn", value = "D70008503", required = true),
            @ApiImplicitParam(paramType = "body", dataType = "String", name = "streamUrl", value = "rtmp://host:port/sn", required = true)
    })
    @PostMapping("/start/recording/stream/camera/{sn}")
    public ResponseEntity recordRTMPStart(HttpServletRequest request,@PathVariable String sn) {

        if(!videoRecordPath_temp.endsWith(File.separator)) videoRecordPath_temp = videoRecordPath_temp + File.separator;

        if(StringUtils.isEmpty(sn))
            return new ResponseEntity(" Please specific the camera sn (should not be null or empty) ", HttpStatus.BAD_REQUEST);

       Map<String,String> rtmpInProgress  = recorderRedisHandler.getRTMPRecorderMap();
        if(rtmpInProgress.keySet().contains(sn+RTMP_SUFIX)){
            String rtmpUrl = rtmpInProgress.get(sn+RTMP_SUFIX);
            return new ResponseEntity("The rtmp stream " + rtmpUrl + " is already being recorded with the SN "
                    + sn, HttpStatus.BAD_REQUEST);
        }

        String reqJson = new RequestHanlder().getRequestJson(request);

        JSONObject jsonObject = null;
        try {
            jsonObject = JSON.parseObject(reqJson);
        } catch (Exception e) {
            return new ResponseEntity("the request string in the request body is malformat json", HttpStatus.BAD_REQUEST);
        }

        String rtmpUrl = "";
        if(jsonObject != null) {
            if(jsonObject.containsKey("streamUrl")){
                rtmpUrl = jsonObject.getString("streamUrl");
            }
            if(StringUtils.isEmpty(rtmpUrl))
                return new ResponseEntity("requested RTMP URL should not be null or empty", HttpStatus.BAD_REQUEST);
            else //ring recording
            {
                String recordPath = videoRecordPath_temp +
                        sn + RTMP_SUFIX + File.separator;
                File folder = new File(recordPath);
                if(!folder.exists())
                    folder.mkdirs();
                String file = recordPath;
                recorderRedisHandler.saveRTMPRecorderStatus(sn+RTMP_SUFIX,rtmpUrl);
                log.info("RTMP -- ring recording for " + recordPath +  "   >>> " +  file);
                ffmpegUtils.rtmpRecorder(rtmpUrl,file,sn+RTMP_SUFIX);
                //return new ResponseEntity("Started to record stream " + sn, HttpStatus.OK);
            }
        }

        return new ResponseEntity("Started to record stream " + sn, HttpStatus.OK);
    }


    @ApiOperation("RTMP 停止录制接口")
    @ApiResponses({ @ApiResponse(code = 200, message = "停止录制成功"),
            @ApiResponse(code = 400, message = "RTMP 停止录制失败")})
    @ApiImplicitParams({ @ApiImplicitParam(paramType = "path", dataType = "String", name = "sn", value = "D70008503", required = true)
    })
    @PostMapping("/stop/recording/stream/camera/{sn}")
    public ResponseEntity recordRTMPStop(HttpServletRequest request,@PathVariable String sn) {

        if(StringUtils.isEmpty(sn))
            return new ResponseEntity(" Please specific the camera sn (should not be null or empty) ", HttpStatus.BAD_REQUEST);
        Map<String,String> requestMap =  recorderRedisHandler.getRTMPRecorderMap();
        if (!requestMap.keySet().stream().anyMatch(s -> s.contains(sn+RTMP_SUFIX))) {
            return new ResponseEntity("The RTMP recording for camera " + sn + " has been stopped ", HttpStatus.BAD_REQUEST);
        }

        String rtmpUrl = requestMap.get(sn+RTMP_SUFIX);
        log.info("stopping rtmp " + rtmpUrl);
//        Map<String, String> stoppingMap = recorderRedisHandler.getStoppingMap();

//        if (stoppingMap.keySet().stream().anyMatch(s -> s.contains(sn+RTMP_SUFIX))) {
//            return new ResponseEntity("The RTMP recording for camera " + sn + " is being stopped, please wait a moment ", HttpStatus.BAD_REQUEST);
//        }

        try {
            handler.stopRTMPRecord(rtmpUrl);
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        recorderRedisHandler.removeRTMPRecorderStatus(sn+RTMP_SUFIX);
        return new ResponseEntity("Successfully stop recording RTMP for camera " + sn, HttpStatus.OK);
    }


//    @Scheduled(cron = "0/2 * * * * ? ")
//    private void bringup() {
//        //log.info("Bring the stream up to recording ");
//        //get all in progress recording streams
//        Map<String, String> requestMap = recorderRedisHandler.getRTMPRecorderMap();
//        if(!videoRecordPath_temp.endsWith(File.separator))videoRecordPath_temp = videoRecordPath_temp + File.separator;
//        requestMap.keySet().forEach(s -> {
//            String rtmpUrl = requestMap.get(s);
//            if (!handler.rtmpRecordInprogress(rtmpUrl)) {
//                handler.stopRTMPRecordForcibly(rtmpUrl);
//                String recordPath = videoRecordPath_temp +
//                        s + File.separator;
//                File folder = new File(recordPath);
//                if (!folder.exists())
//                    folder.mkdirs();
//                String file = recordPath;
//                log.info("bring the rtmp up to recording " + recordPath + "   >>> " + file);
//                ffmpegUtils.rtmpRecorder(rtmpUrl, file, s);
//            }
//
//        });
//    }

//    @Scheduled(cron = "0 0/1 * * * ? ")
//    private void removeUnnessaryRawReady(){
//        //log.info("handling historical rtmp record files with maximum retention hours " + rtmpRecordHistoricalMax);
//
//        //retention million seconds
//        long retention = rtmpRecordHistoricalMax * 60 * 60 * 1000L;
//
//        //get all in progress recording streams
//        Map<String,String> requestMap =  recorderRedisHandler.getRTMPRecorderMap();
//        requestMap.keySet().forEach(s->{
//            if(!videoRecordPath_temp.endsWith(File.separator))videoRecordPath_temp = videoRecordPath_temp + File.separator;
//            String recordPath = videoRecordPath_temp +
//                    s + File.separator;
//            if(new File(recordPath).exists()){
//                log.info("handling historical rtmp record files with maximum retention hours " + rtmpRecordHistoricalMax);
//                log.info("entering into folder " + recordPath);
//                Arrays.stream(new File(recordPath).listFiles()).filter(f->StringUtils.containsIgnoreCase(f.getName(),"raw-ready")
//                && StringUtils.containsIgnoreCase(f.getName(),".mp4")
//                ).forEach(f->{
////                && !recorderRedisHandler.getStoppingMap().keySet().contains(s)).forEach(f->{
//                    long lastModify = f.lastModified();
//                    long created = Long.valueOf(StringUtils.substringBetween(f.getName(),"raw-ready",".mp4"));
//                    long last_e = lastModify + retention;
//                    long last_s = created + retention;
//                    if(last_s < System.currentTimeMillis() && last_e < System.currentTimeMillis()){
//                        log.info("delete the file " + f.getName() + "created retention "+ last_s + " last modify retention "+ last_s) ;
//                        f.delete();
//                    }
//                });
//            }
//        });
//    }

}
