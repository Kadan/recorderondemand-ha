package com.ilabservice.darwinrecorder.hlslive.measurements;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

import java.time.Instant;

@Measurement(name="DARWIN_EVENT_MEASUREMENTS")
public class DarwinMeasurement {
    public String getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = String.valueOf(time);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTotalStreams() {
        return totalStreams;
    }

    public void setTotalStreams(int totalStreams) {
        this.totalStreams = totalStreams;
    }

    public int getClusterSize() {
        return clusterSize;
    }

    public void setClusterSize(int clusterSize) {
        this.clusterSize = clusterSize;
    }


    @Column(name="time")
    private String time;
    @Column(name="type")
    private String type;
    @Column(name="totalStreams")
    private int totalStreams;
    @Column(name="clusterSize")
    private int clusterSize;


}
