package com.ilabservice.darwinrecorder.hlslive.measurements;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

import java.time.Instant;

@Measurement(name = "RECORDER_MEASUREMENTS")
public class RecorderMeasurement {
    public String getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = String.valueOf(time);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRecordedHours() {
        return recordedHours;
    }

    public void setRecordedHours(double recordedHours) {
        this.recordedHours = recordedHours;
    }

    @Column(name="time")
    private String time;
    @Column(name="type")
    private String type;
    @Column(name="event")
    private String event;
    @Column(name="name")
    private String name;
    @Column(name="recordedHours")
    private double recordedHours;

    public int getRecordingCount() {
        return recordingCount;
    }

    public void setRecordingCount(int recordingCount) {
        this.recordingCount = recordingCount;
    }

    @Column(name="recordingCount")
    private int recordingCount;

    public int getRecords() {
        return records;
    }

    public void setRecords(int records) {
        this.records = records;
    }

    @Column(name="records")
    private int records;
}
