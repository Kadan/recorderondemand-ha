package com.ilabservice.darwinrecorder.hlslive.cron;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.dangdang.elasticjob.lite.annotation.ElasticSimpleJob;
import com.ilabservice.darwinrecorder.hlslive.ffmpeg.FFMPEGUtils;
import com.ilabservice.darwinrecorder.hlslive.util.Handler;
import com.ilabservice.darwinrecorder.hlslive.util.RecorderRedisHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Map;

@ElasticSimpleJob(cron = "0/2 * * * * ? ", jobName = "recoerderdaemon",shardingTotalCount = 1, jobParameter = "recoerderdaemon",shardingItemParameters = "0=A,1=B,2=C,3=D")
@Component
public class RecorderDaemon implements SimpleJob {
    @Value("${videoRecordPath-temp}")
    private String videoRecordPath_temp;
    @Autowired
    RecorderRedisHandler recorderRedisHandler;
    @Autowired
    Handler handler;
    @Autowired
    FFMPEGUtils ffmpegUtils;

    private static final Logger log = LoggerFactory.getLogger(RecorderDaemon.class);
    @Override
    public void execute(ShardingContext shardingContext) {
        //////log.info("Running rtmp recorder daemon ... ");
        //get all in progress recording streams
        Map<String, String> requestMap = recorderRedisHandler.getRTMPRecorderMap();
        if(!videoRecordPath_temp.endsWith(File.separator))videoRecordPath_temp = videoRecordPath_temp + File.separator;
        requestMap.keySet().forEach(s -> {
            String rtmpUrl = requestMap.get(s);
            if (!handler.rtmpRecordInprogress(rtmpUrl)) {
                handler.stopRTMPRecordForcibly(rtmpUrl);
                String recordPath = videoRecordPath_temp +
                        s + File.separator;
                File folder = new File(recordPath);
                if (!folder.exists())
                    folder.mkdirs();
                String file = recordPath;
                log.info("bring the rtmp up to recording " + recordPath + "   >>> " + file);
                ffmpegUtils.rtmpRecorder(rtmpUrl, file, s);
            }

        });

    }
}
