package com.ilabservice.darwinrecorder.hlslive.cron;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.dangdang.elasticjob.lite.annotation.ElasticSimpleJob;
import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
import com.ilabservice.darwinrecorder.hlslive.config.MQTTConfiguration;
import com.ilabservice.darwinrecorder.hlslive.ffmpeg.FFMPEGUtils;
import com.ilabservice.darwinrecorder.hlslive.service.VideoService;
import com.ilabservice.darwinrecorder.hlslive.util.Handler;
import com.ilabservice.darwinrecorder.hlslive.util.RecorderRedisHandler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@ElasticSimpleJob(cron = "0/10 * * * * ? ", jobName = "processdaemon",shardingTotalCount = 1, jobParameter = "processdaemon",shardingItemParameters = "0=A,1=B,2=C,3=D")
@Component
public class RecorderProcessDaemon implements SimpleJob {


    @Autowired
    VideoService videoService;

    @Value("${mqtt.producer.sendMessage}")
    private boolean sendMsg;

    @Autowired
    RecorderRedisHandler recorderRedisHandler;

    @Value("${mqtt.producer.default-topic}")
    private String defaultTopic;

    @Value("${videoRecordPath}")
    private String videoRecordPath;

    @Value("${fileDownloadPath}")
    private String fileDownloadPath;

    @Value("${videoRecordPath-temp}")
    private String videoRecordPath_temp;

    @Autowired
    InfluxDbUtils influxDbUtils;

    @Autowired
    Handler handler;

    @Autowired
    Map<String,Long> recoerderFileSizeChecking;

    @Autowired
    Map<String,String> recoerderFileNameChecking;

    @Autowired
    FFMPEGUtils ffmpegUtils;


    private static ThreadFactory rtspDaemonFactory = new ThreadFactory() {
        private final AtomicInteger counter = new AtomicInteger();

        @Override
        public Thread newThread(Runnable runable) {
            return new Thread(runable, "### rtspDaemonFactory corn thread ### " + counter.getAndIncrement());
        }
    };

    private static ThreadPoolExecutor rtspDaemonExecutor = new ThreadPoolExecutor(300, 2000, 3, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100), rtspDaemonFactory);


    private static ThreadFactory rtmpDaemonFactory = new ThreadFactory() {
        private final AtomicInteger counter = new AtomicInteger();

        @Override
        public Thread newThread(Runnable runable) {
            return new Thread(runable, "### rtmpDaemonFactory corn thread ### " + counter.getAndIncrement());
        }
    };

    private static ThreadPoolExecutor rtmpDaemonExecutor = new ThreadPoolExecutor(300, 2000, 3, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100), rtmpDaemonFactory);


    private static final Logger log = LoggerFactory.getLogger(RecorderProcessDaemon.class);



    //@Scheduled(initialDelay = 10000,fixedRate = 10000)
    private void recordDaemon(){
        Map<String,String> rtmpRecorderMap = recorderRedisHandler.getRTMPRecorderMap();
        Map<String,Long> rtspRecorderMap = recorderRedisHandler.getRecorderMap();

        if(!videoRecordPath_temp.endsWith(File.separator))videoRecordPath_temp = videoRecordPath_temp + File.separator;

        rtmpRecorderMap.keySet().stream().filter(s->handler.rtmpRecordInprogress(rtmpRecorderMap.get(s))).forEach(s->{
            rtmpDaemonExecutor.submit(
                    new Callable<Integer>() {
                        @Override
                        public Integer call() {
                            String rtmpUrl = rtmpRecorderMap.get(s);
                            String path = videoRecordPath_temp + s;
                            log.info("checking rtmpurl's recording status " + rtmpUrl + " in folder " + path);
                            return checkCurrentFileSize(rtmpUrl,path,"rtmp",s);
                            //return 0;
                        }
                    }
            );
        });

        rtspRecorderMap.keySet().stream().filter(r->handler.streamLive(r)).forEach(r->{
            rtspDaemonExecutor.submit(
                    new Callable<Integer>() {
                        @Override
                        public Integer call() {
                            String path = videoRecordPath_temp + r;
                            log.info("checking rtsp's recording status " + r + " in folder " + path+"-manual");
                            return checkCurrentFileSize(r,path+"-manual","rtsp",r);
                            //return 0;
                        }
                    }
            );
        });
    }



    public int checkCurrentFileSize(String stream,String path,String type,String key){
        File folder = new File(path);
        long preSize = 0;
        if(recoerderFileSizeChecking.keySet().size() ==0) preSize =0;
        else if(recoerderFileSizeChecking.keySet().contains(key))
            preSize = recoerderFileSizeChecking.get(key);
//        log.info("the previous file size is " + preSize);

        if(folder.exists()){
            File[] files = folder.listFiles();
            if(files != null && files.length > 0){
                List<File> currentFiles = Arrays.stream(files).filter(f->
                        StringUtils.endsWith(f.getName(),"mp4")&&
                                StringUtils.containsIgnoreCase(f.getName(),"raw-ready")
                ).collect(Collectors.toList());

                if(currentFiles !=null && currentFiles.size()>0){
                    Collections.sort(currentFiles);
                    File currentFile = currentFiles.get(currentFiles.size()-1);
                    String currentFileName = currentFile.getName();
                    String preFileName = null;
                    if(recoerderFileNameChecking.containsKey(key))
                        preFileName = recoerderFileNameChecking.get(key);
                    //the very first time
                    if(preFileName == null){
                        log.info("there is no comparision before  ");
                        recoerderFileNameChecking.put(key,currentFileName);
                        return 0;
                    }
                    //file changed
                    if(!StringUtils.equalsIgnoreCase(currentFileName,preFileName)){
                        log.info("the file has switched  from  " + preFileName + " to " + currentFileName);
                        recoerderFileNameChecking.put(key,currentFileName);
                    }

                    if(StringUtils.equalsIgnoreCase(currentFileName,preFileName)){
//                        log.info("it is still the file  " + currentFileName);
                        Long size = currentFile.length();
//                        log.info("the current file size is " + size);
                        if(preSize <=0){
                            recoerderFileSizeChecking.put(key,size);
                        }else{
                            if(size != preSize){
                                recoerderFileSizeChecking.put(key,size);
                                return 0;
                            }
                            else if(size == preSize){
                                log.info("The file size stop growing, stop the process  " + stream);
                                if(type.equalsIgnoreCase("rtmp"))
                                    handler.stopRTMPRecord(stream);
                                if(type.equalsIgnoreCase("rtsp"))
                                    handler.Stop(key);
                            }
                        }
                    }else
                        return 0;
                }
            }
        }else
            return 1;

        return 0;

    }

    @Override
    public void execute(ShardingContext shardingContext) {
        //log.info("Running the process checking (base on file sizing) daemon ... ");
        recordDaemon();
    }
}
