package com.ilabservice.darwinrecorder.hlslive.cron;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.dangdang.elasticjob.lite.annotation.ElasticSimpleJob;
import com.ilabservice.darwinrecorder.hlslive.controller.StreamRecorderController;
import com.netflix.appinfo.InstanceInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
@ElasticSimpleJob(cron = "0/3 * * * * ?", jobName = "darwinjob",shardingTotalCount = 6, jobParameter = "darwinparameter",shardingItemParameters = "0=A,1=B,2=C,3=D")
@Component
public class StreamDistributionCron implements SimpleJob {
    private static final Logger log = LoggerFactory.getLogger(StreamDistributionCron.class);

    @Autowired
    DiscoveryClient discoveryClient;

    @Autowired
    RestTemplate darwinRestTemplate;

    @Autowired
    List<String> allRTSP;

    @Autowired
    List<String> darwinList;

    @Autowired
    StreamRecorderController streamRecorderController;

    @Override
    public void execute(ShardingContext shardingContext) {
        int shards = shardingContext.getShardingTotalCount();
        List<String> allStreams = new ArrayList<String>();
        List<String> instanceInfos = discoveryClient.getServices();
        int current = shardingContext.getShardingItem();

        if(instanceInfos!=null && instanceInfos.size()>0){
            instanceInfos.stream().forEach(i-> {
                if (StringUtils.startsWithIgnoreCase(i, "darwin-service-")) {
                    if(!darwinList.contains(i))
                        darwinList.add(i);
                    String pusherString = darwinRestTemplate.getForObject("http://" + i + "/api/v1/pushers", String.class);
                    JSONObject jsonObject = JSONObject.parseObject(pusherString);
                    int total = jsonObject.getIntValue("total");
                    if (total > 0) {
                        JSONArray jSONArray = jsonObject.getJSONArray("rows");
                        jSONArray.stream().map(jo -> {
                            JSONObject jo1 = (JSONObject) jo;
                            return jo1;
                        }).forEach(s1 -> {
                            allStreams.add(s1.getString("source"));
                            if(!allRTSP.contains(s1.getString("source").trim()))
                                allRTSP.add(s1.getString("source").trim());
                        });
                    }
                }
            });
        }
        log.info("current shard is  " + current + " of " + shards);
        Collections.sort(allStreams);
        List<List<String>> lists = new ListUtils().averageAssign(allStreams,shards);
        List<String> currentList = lists.get(current);
        if(currentList !=null && currentList.size()>0){
            currentList.stream().forEach(s->{
                //log.info(" adding stream ...  " + s);
                //streamRecorderController.ring(s);
            });
        }

    }

    public class ListUtils {
        public <T> List<List<T>> averageAssign(List<T> source, int n) {
            List<List<T>> result = new ArrayList<List<T>>();
            int remaider = source.size() % n;
            int number = source.size() / n;
            int offset = 0;
            for (int i = 0; i < n; i++) {
                List<T> value = null;
                if (remaider > 0) {
                    value = source.subList(i * number + offset, (i + 1) * number + offset + 1);
                    remaider--;
                    offset++;
                } else {
                    value = source.subList(i * number + offset, (i + 1) * number + offset);
                }
                result.add(value);
            }
            return result;
        }
    }
}
