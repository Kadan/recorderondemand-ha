package com.ilabservice.darwinrecorder.hlslive.cron;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.dangdang.elasticjob.lite.annotation.ElasticSimpleJob;
import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
import com.ilabservice.darwinrecorder.hlslive.config.MQTTConfiguration;
import com.ilabservice.darwinrecorder.hlslive.ffmpeg.FFMPEGUtils;
import com.ilabservice.darwinrecorder.hlslive.service.VideoService;
import com.ilabservice.darwinrecorder.hlslive.util.Handler;
import com.ilabservice.darwinrecorder.hlslive.util.RecorderRedisHandler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@ElasticSimpleJob(cron = "0 0/1 * * * ? ", jobName = "housekeep",shardingTotalCount = 1, jobParameter = "housekeep",shardingItemParameters = "0=A,1=B,2=C,3=D")
@Component
public class RemoveHistoricalFileCorn implements SimpleJob {
    @Value("${rtmpRecordHistoricalMax}")
    private int rtmpRecordHistoricalMax;
    @Value("${rtspRecordHistoricalMax}")
    private int rtspRecordHistoricalMax;
    @Value("${videoRecordPath-temp}")
    private String videoRecordPath_temp;
    @Autowired
    RecorderRedisHandler recorderRedisHandler;
    @Autowired
    List<String> allRTSP;

    private static final Logger log = LoggerFactory.getLogger(RemoveHistoricalFileCorn.class);
    @Override
    public void execute(ShardingContext shardingContext) {
        ////log.info("Running the house keep ... for historical recorded file in temp folder ");
        ////CLEANING THE RTSP /////

        Map<String, Long> requestMaprtsp = recorderRedisHandler.getRecorderMap();
        long retentionRTSP = rtspRecordHistoricalMax * 60 * 60 * 1000L;
        List<String> fetchList = allRTSP;
        fetchList.stream().forEach(s->{
            String recordPath = videoRecordPath_temp + File.separator +
                    org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/") + "-manual" + File.separator;
            File folder = new File(recordPath);
            if(requestMaprtsp.entrySet().size()==0 || !requestMaprtsp.keySet().stream().anyMatch(ss->ss.equalsIgnoreCase(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/")))) {
                new Thread(() ->{
                    //remove unnecessary raw-ready
                    //String currentFile = ffmpegUtils.currentFile(recordPath);
                    if(folder !=null && folder.isDirectory()){
                        Arrays.stream(folder.listFiles()).filter(sss->sss.isFile()&&sss.getName().endsWith("mp4")
                                //&& !sss.getName().equalsIgnoreCase(currentFile)
                                && sss.getName().contains("raw-ready")).forEach(sss->{
                            log.info(s + "No record request ... Removing unnecessary segmentation file " + sss.getName());
                            long created = Long.valueOf(StringUtils.substringBetween(sss.getName(),"raw-ready",".mp4"));
                            long last_s = created + retentionRTSP;
                            if(last_s<System.currentTimeMillis())
                                sss.delete();
                        });
                    }

                }).start();
            }
        });

        ///////cleaning the RTMP ////////
        long retention = rtmpRecordHistoricalMax * 60 * 60 * 1000L;
        //get all in progress recording streams
        Map<String,String> requestMap =  recorderRedisHandler.getRTMPRecorderMap();
        requestMap.keySet().forEach(s->{
            if(!videoRecordPath_temp.endsWith(File.separator))videoRecordPath_temp = videoRecordPath_temp + File.separator;
            String recordPath = videoRecordPath_temp +
                    s + File.separator;
            if(new File(recordPath).exists()){
                log.info("handling historical rtmp record files with maximum retention hours " + rtmpRecordHistoricalMax);
                log.info("entering into folder " + recordPath);
                Arrays.stream(new File(recordPath).listFiles()).filter(f-> StringUtils.containsIgnoreCase(f.getName(),"raw-ready")
                        && StringUtils.containsIgnoreCase(f.getName(),".mp4")
                ).forEach(f->{
//                && !recorderRedisHandler.getStoppingMap().keySet().contains(s)).forEach(f->{
                    long lastModify = f.lastModified();
                    long created = Long.valueOf(StringUtils.substringBetween(f.getName(),"raw-ready",".mp4"));
                    long last_e = lastModify + retention;
                    long last_s = created + retention;
                    if(last_s < System.currentTimeMillis() && last_e < System.currentTimeMillis()){
                        log.info("delete the file " + f.getName() + "created retention "+ last_s + " last modify retention "+ last_s) ;
                        f.delete();
                    }
                });
            }
        });

    }
}
